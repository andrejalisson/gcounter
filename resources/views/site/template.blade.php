<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Transformando Dados em Informações relevantes para tomada de decisão dos clientes.">
    <meta name="author" content="André Jálisson - 85 985965372">
    <title>{{$title}} - GCounter Inteligência de Negócios</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i%7CRaleway:600,800" rel="stylesheet">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" href="/revolution/css/settings.css">
    <link rel="stylesheet" href="/revolution/css/layers.css">
    <link rel="stylesheet" href="/revolution/css/navigation.css">
    <!-- ARCHIVES CSS -->
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/lightcase.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/colors/purple.css">
    <link rel="stylesheet" id="blue" href="/css/color/default.css">
</head>

<body>
    
    @yield('corpo')
    

    <!-- START FOOTER -->
    <footer class="first-footer" id="Contato">
        <div class="top-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="netabout">
                            <a href="/" class="logo">
                                <img src="images/logo-white.png" alt="netcom">
                            </a>
                            <p>INTELIGÊNCIA DE NEGÓCIOS</p>
                        </div>
                        <ul class="netsocials">
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://likedin.com/company/gcounter"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="https://instagram.com/gcounterbrasil"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="contactus">
                            <h3>Contato</h3>
                            <ul>
                                <li>
                                    <div class="info">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <p class="in-p">Av.Santos Dumont, 2456 - Sala 406.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <p class="in-p"> (85) 3393-7616</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <p class="in-p ti">consultoria@gcounter.com.br</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="info">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <p class="in-p ti">8:00 - 17:00.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="widget">
                            <h3>Instagram</h3>
                            <div style="max-width: 250px;" class="elfsight-app-9df17316-43c9-427b-8c33-636b65dc0ffe"></div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="newsletters">
                            <h3>Receba nosso conteúdo.</h3>
                            <p>Fique por dentro de tudo que acontece no mundo dos negócios.</p>
                        </div>
                        <form class="bloq-email mailchimp form-inline" method="post">
                            <label for="Digite seu email" class="error"></label>
                            <div class="email">
                                <input type="email" id="subscribeEmail" name="EMAIL" placeholder="Digite seu email">
                                <input type="submit" value="Inscrever">
                                <p class="subscription-success"></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="second-footer">
            <div class="container">
                <p>{{date('Y')}} © GCounter Inteligência de Negócios - CNPJ:35.038.940/0001-54</p>
                <p>Feito por  <i class="fa fa-heart" aria-hidden="true"></i> André Jálisson (85) 9 8596-5372.</p>
            </div>
        </div>
    </footer>

    <a data-scroll href="#heading" class="go-up"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
    <!-- END FOOTER -->

    <!-- START PRELOADER -->
    <div id="preloader">
        <div id="status">
            <div class="status-mes"></div>
        </div>
    </div>
    <!-- END PRELOADER -->
    
    <!-- ARCHIVES JS -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/tether.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/transition.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/particles.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/jquery.waypoints.min.js"></script>
    <script src="/js/jquery.counterup.min.js"></script>
    <script src="/js/imagesloaded.pkgd.min.js"></script>
    <script src="/js/isotope.pkgd.min.js"></script>
    <script src="/js/smooth-scroll.min.js"></script>
    <script src="/js/lightcase.js"></script>
    <script src="/js/owl.carousel.js"></script>
    <script src="/js/ajaxchimp.min.js"></script>
    <script src="/js/newsletter.js"></script>
    <script src="/js/jquery.form.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/forms-2.js"></script>

    <!-- Slider Revolution scripts -->
    <script src="/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="https://apps.elfsight.com/p/platform.js" defer></script>
    <!-- MAIN JS -->
    <script src="/js/inner-2.js"></script>

</body>

</html>
