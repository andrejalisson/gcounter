@extends('site.template')

@section('corpo')

<header class="heading-6 fixed-top" id="heading">
    <div class="container">
        <a href="/" class="logo">
            <img src="/images/logo-brown.png" alt="financetop">
        </a>
        <button type="button" class="button-menu hidden-lg-up" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <div class="get-quote hidden-lg-down">
            <a target="_blank" href="https://wa.me/5585999505692">
                <p>Agende uma visita</p>
            </a>
        </div>
        <nav id="main-menu" class="collapse">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/Sobre">Sobre</a></li>
                <li><a data-scroll href="#Servicos">Serviços</a></li>
                <li><a href="/Portfolio">Portfólio</a></li>
                <li><a data-scroll href="#Contato">Contato</a></li>
            </ul>
        </nav>
    </div>
</header>

    <section style="max-height: 150px;" class="headings">
        <div class="text-heading text-center">
            
        </div>
    </section>
<!-- START SECTION ABOUT -->
<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 who">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/AseTOGU5E_A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-lg-6 col-md-12 who-1">
                <div>
                    <h2 class="text-left mb-4">DASHBOARDS DE ANÁLISES DE VENDAS DE <span>CERVEJARIA</span></h2>
                </div>
                <div class="pftext">
                    <p>Dashboards de Análise de vendas de Cervejaria com On Tap e Delivery e várias análises:</p>

                    <p>Vendas, Lucro, Custo, Cobertura de Clientes, Margem de lucro %, Análise de lucratividade, Comparativo de desempenho de venda On Tap x Delivery, Análise de Recorrência de Clientes, Análise de Clientes. As Dashboards possuem vários filtros para análise de cenários.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 who-1">
                <div>
                    <h2 class="text-left mb-4">DASHBOARDS DE <span>ANÁLISES DE VENDAS</span>, FINANCEIRO E COM SIMULADOR DE CENÁRIOS</h2>
                </div>
                <div class="pftext">
                    <p>Dashboards de Vendas, Financeiro com Simulador de Cenários e várias análises:</p>

                    <p>Vendas, Lucro, Margem de lucro %, Margem por Fornecedor, Receita Por Linha de Produto, Análise por Equipe de Vendas, Fluxo de Caixa, Simulador de Cenário. As Dashboards possuem vários filtros para análise de cenários.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 who">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/43JRZIHNpvI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 who">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Fe2J3_J9VN4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-lg-6 col-md-12 who-1">
                <div>
                    <h2 class="text-left mb-4">DASHBOARDS DE ANÁLISES DE VENDAS DE <span>HAMBURGUERIA</span></h2>
                </div>
                <div class="pftext">
                    <p>Dashboards de Análise de vendas de Hamburgueria com Loja Física, Delivery e várias análises:</p>

                    <p>Vendas, Lucro, Custo, Cobertura de Clientes, Margem de lucro %, Análise de lucratividade, Comparativo de desempenho de venda Loja Física x Delivery, Análise de Recorrência de Clientes, Análise de Clientes. As Dashboards possuem vários filtros para análise de cenários.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 who-1">
                <div>
                    <h2 class="text-left mb-4">DASHBOARDS DE ANÁLISES DE <span>LOGÍSTICA</span></h2>
                </div>
                <div class="pftext">
                    <p>Dashboards de Logística com Análise de Fretes, Frota, Veículos e várias outras análises:</p>

                    <p>Custo por Frete, Qtd de Viagens, Peso, Valor das mercadorias transportadas, Valor de Frete Líquido por UF, Análises por UF, Custo com Frota e diversos indicadores, Análise de Veículos. As Dashboards possuem vários filtros para análise de cenários.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 who">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/7YXFY0WunqU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 who">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/YtxAIwZGUz8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-lg-6 col-md-12 who-1">
                <div>
                    <h2 class="text-left mb-4">DASHBOARDS DE ANÁLISE DE VENDAS DE <span>RESTAURANTE JAPONÊS</span></h2>
                </div>
                <div class="pftext">
                    <p>Dashboards de Análise de vendas de Restaurante Japonês com várias análises: </p>

                    <p>Vendas, Lucro, Custo, Cobertura de Clientes, Margem de lucro %, Análise de lucratividade, Comparativo de desempenho de venda por Loja, Análise de Recorrência de Clientes, Análise de Clientes. As Dashboards possuem vários filtros para análise de cenários.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="who-we-are">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 who-1">
                <div>
                    <h2 class="text-left mb-4">DASHBOARD DE ANÁLISE DE <span>FINANCEIRO</span></h2>
                </div>
                <div class="pftext">
                    <p>Dashboards Financeiro com várias análises: </p>

                    <p>Valores a Receber, Valores a Pagar, Qtd de Pagamentos, Qtd de Recebimentos, Saldo de Caixa, Comparativo de Entradas e Saídas, Recebimentos por UF, Pagamentos por Fornecedor, Qtd Pagamentos em Atraso e no Prazo, Qtd Recebimentos em Atraso e no Prazo. As Dashboards possuem vários filtros para análise de cenários.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 who">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Oq8Ti_PVtXw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
<!-- END SECTION ABOUT -->

@endsection